DROP TABLE IF EXISTS employee;
DROP TABLE IF EXISTS company;

CREATE TABLE company
(
 pk_company_id SERIAL PRIMARY KEY,
 title VARCHAR(50) NOT NULL,
 tax_identification_number VARCHAR(10) NOT NULL,
 address VARCHAR(100) NOT NULL,
 phone_number VARCHAR(15) NOT NULL
);

CREATE TABLE employee
(
 pk_employee_id SERIAL PRIMARY KEY,
 full_name VARCHAR(50) NOT NULL,
 date_of_birth DATE NOT NULL,
 email VARCHAR(50) NOT NULL,
 fk_company_id INTEGER NOT NULL,
 FOREIGN KEY (fk_company_id) REFERENCES company (pk_company_id) ON DELETE CASCADE
);