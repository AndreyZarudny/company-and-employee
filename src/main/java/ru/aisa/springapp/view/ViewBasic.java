package ru.aisa.springapp.view;

public interface ViewBasic {

  void init();

  void addEvent();

  void editEvent();

  void deleteEvent();

  void searchEvent();

  void showEvent();
}
