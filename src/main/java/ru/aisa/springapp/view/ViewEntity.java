package ru.aisa.springapp.view;

import com.vaadin.flow.component.grid.Grid;

public interface ViewEntity<E> {

  void addEntity();

  void editEntity(E e);

  void deleteEntity(E e);

  void showEntity(String data);

  Grid<E> getGrid();

  void setupGrid(Grid<E> grid);

  void init();

  void setVisible(boolean visible);
}
