package ru.aisa.springapp.view;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import java.util.HashMap;
import java.util.Map;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import ru.aisa.springapp.component.ChangeHandler;
import ru.aisa.springapp.entity.Company;

@Route("")
@CssImport("./styles/shared-styles.css")
public class MainView extends VerticalLayout implements ViewBasic {

  CompanyView companyView;
  EmployeeView employeeView;

  Button addBtn = new Button("+Добавить");
  Button editBtn = new Button("Редактировать");
  Button deleteBtn = new Button("-Удалить");
  Button showBtn = new Button("Показать сотрудников");
  TextField search = new TextField("", "Поиск");

  HorizontalLayout toolbar = new HorizontalLayout(addBtn, editBtn, deleteBtn, showBtn, search);

  Tab companyTab = new Tab("Компании");
  Tab employeeTab = new Tab("Сотрудники");

  Tabs tabs = new Tabs(companyTab, employeeTab);

  ViewEntity selectedView;

  @Setter
  ChangeHandler changeHandler;

  @Autowired
  public MainView(CompanyView companyView, EmployeeView employeeView) {
    this.companyView = companyView;
    this.employeeView = employeeView;

    showBtn.addClassName("show-btn");
    search.addClassName("search-tf");

    add(toolbar, tabs);
    add(this.companyView, this.employeeView);

    init();

    addEvent();
    editEvent();
    deleteEvent();
    searchEvent();
    showEvent();

    setChangeHandler(() -> selectedView.showEntity(""));
  }

  @Override
  public void init() {
    selectedView = companyView;

    selectedView.setVisible(true);
    employeeView.setVisible(false);

    search.setValueChangeMode(ValueChangeMode.EAGER);

    Map<Tab, ViewEntity> tabsToViews = new HashMap<>();
    tabsToViews.put(companyTab, companyView);
    tabsToViews.put(employeeTab, employeeView);

    tabs.addSelectedChangeListener(event -> {
      selectedView.getGrid().deselectAll();
      tabsToViews.values().forEach(view -> view.setVisible(false));
      selectedView = tabsToViews.get(tabs.getSelectedTab());
      selectedView.setVisible(true);

      showBtn.setVisible(selectedView != employeeView);

      changeHandler.onChange();
      search.clear();
      searchEvent();
    });
  }

  @Override
  public void addEvent() {
    addBtn.addClickListener(click -> selectedView.addEntity());
  }

  @Override
  public void editEvent() {
    editBtn.addClickListener(click -> {
      if (selectedView.getGrid().getSelectionModel().getFirstSelectedItem().isPresent()) {
        selectedView.editEntity(
            selectedView.getGrid().getSelectionModel().getFirstSelectedItem().get());
      }
    });
  }

  @Override
  public void deleteEvent() {
    deleteBtn.addClickListener(click -> {
      if (selectedView.getGrid().getSelectionModel().getFirstSelectedItem().isPresent()) {
        selectedView.deleteEntity(
            selectedView.getGrid().getSelectionModel().getFirstSelectedItem().get());
        changeHandler.onChange();
      }
    });
  }

  @Override
  public void searchEvent() {
    search.addValueChangeListener(enter -> selectedView.showEntity(enter.getValue()));
  }

  @Override
  public void showEvent() {
    showBtn.addClickListener(click -> {
      if (companyView.getGrid().getSelectionModel().getFirstSelectedItem().isPresent()) {
        Company company = companyView.getGrid().getSelectionModel().getFirstSelectedItem().get();
        companyView.setVisible(false);
        showBtn.setVisible(false);
        tabs.setSelectedTab(employeeTab);
        employeeView.setVisible(true);
        companyView.grid.deselectAll();
        changeHandler.onChange();
        employeeView.showEmployeeCompany(company);
      }
    });
  }
}
