package ru.aisa.springapp.view;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;
import ru.aisa.springapp.entity.Company;
import ru.aisa.springapp.form.CompanyForm;
import ru.aisa.springapp.repository.CompanyRepository;

@SpringComponent
@UIScope
public class CompanyView extends VerticalLayout implements ViewEntity<Company> {

  CompanyRepository repository;
  Grid<Company> grid = new Grid<>(Company.class);
  CompanyForm form;

  @Autowired
  public CompanyView(CompanyRepository repository, CompanyForm form) {
    this.repository = repository;
    this.form = form;

    setupGrid(grid);
    init();
    add(grid);

    showEntity("");
    this.form.setChangeHandler(() -> showEntity(""));
  }

  @Override
  public void addEntity() {
    editEntity(new Company());
  }

  @Override
  public void editEntity(Company company) {
    form.edit(company);
  }

  @Override
  public void deleteEntity(Company company) {
    if (company != null) {
      repository.delete(company);
      UI.getCurrent().getPage().reload();
    }
  }

  @Override
  public void showEntity(String data) {
    if (data.isEmpty()) {
      grid.setItems(repository.findAll());
    } else {
      grid.setItems(repository.findByData(data));
    }
  }

  @Override
  public Grid<Company> getGrid() {
    return grid;
  }

  @Override
  public void setupGrid(Grid<Company> grid) {
    grid.removeAllColumns();
    grid.addColumn("id").setHeader("Идентификатор");
    grid.addColumn("title").setHeader("Название компании");
    grid.addColumn("taxIdentificationNumber").setHeader("ИНН");
    grid.addColumn("address").setHeader("Адрес");
    grid.addColumn("phoneNumber").setHeader("Номер").setAutoWidth(true);
  }

  @Override
  public void init() {
    grid.setSizeFull();
    grid.setHeightByRows(true);
    grid.setSelectionMode(SelectionMode.SINGLE);
  }
}
