package ru.aisa.springapp.view;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;
import ru.aisa.springapp.entity.Company;
import ru.aisa.springapp.entity.Employee;
import ru.aisa.springapp.form.EmployeeForm;
import ru.aisa.springapp.repository.EmployeeRepository;

@SpringComponent
@UIScope
public class EmployeeView extends VerticalLayout implements ViewEntity<Employee> {

  EmployeeRepository repository;
  Grid<Employee> grid = new Grid<>(Employee.class);
  EmployeeForm form;

  @Autowired
  public EmployeeView(EmployeeRepository repository, EmployeeForm form) {
    this.repository = repository;
    this.form = form;

    setupGrid(grid);
    init();
    add(grid);

    showEntity("");
    this.form.setChangeHandler(() -> showEntity(""));
  }

  @Override
  public void addEntity() {
    editEntity(new Employee());
  }

  @Override
  public void editEntity(Employee employee) {
    form.edit(employee);

  }

  @Override
  public void deleteEntity(Employee employee) {
    if (employee != null) {
      repository.delete(employee);
    }
  }

  @Override
  public void showEntity(String data) {
    if (data.isEmpty()) {
      grid.setItems(repository.findAll());
    } else {
      grid.setItems(repository.findByData(data));
    }
  }

  @Override
  public Grid<Employee> getGrid() {
    return grid;
  }

  @Override
  public void setupGrid(Grid<Employee> grid) {
    grid.removeAllColumns();
    grid.addColumn("id").setHeader("Идентификатор");
    grid.addColumn("fullName").setHeader("ФИО");
    grid.addColumn("dateOfBirthString").setHeader("Дата рождения");
    grid.addColumn("email").setHeader("Электронная почта");
    grid.addColumn("companyTitle").setHeader("Название компании").setAutoWidth(true);
  }

  @Override
  public void init() {
    grid.setSizeFull();
    grid.setHeightByRows(true);
    grid.setSelectionMode(SelectionMode.SINGLE);
  }

  public void showEmployeeCompany(Company company) {
    grid.setItems(repository.findByCompany(company.getTitle()));
  }
}
