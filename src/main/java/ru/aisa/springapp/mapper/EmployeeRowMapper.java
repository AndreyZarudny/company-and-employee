package ru.aisa.springapp.mapper;

import com.vaadin.flow.data.binder.ValueContext;
import com.vaadin.flow.data.converter.LocalDateToDateConverter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import org.springframework.jdbc.core.RowMapper;
import ru.aisa.springapp.entity.Employee;

public class EmployeeRowMapper implements RowMapper<Employee> {

  @Override
  public Employee mapRow(ResultSet resultSet, int i) throws SQLException {
    Employee employee = new Employee();
    employee.setId(resultSet.getInt("pk_employee_id"));
    employee.setFullName(resultSet.getString("full_name"));
    employee.setDateOfBirthString(new LocalDateToDateConverter()
        .convertToPresentation(resultSet.getDate("date_of_birth"),
            new ValueContext()).format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
    employee.setEmail(resultSet.getString("email"));
    employee.setCompanyTitle(resultSet.getString("title"));
    return employee;
  }
}
