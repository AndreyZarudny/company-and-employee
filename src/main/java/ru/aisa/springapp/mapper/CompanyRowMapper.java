package ru.aisa.springapp.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import ru.aisa.springapp.entity.Company;

public class CompanyRowMapper implements RowMapper<Company> {

  @Override
  public Company mapRow(ResultSet resultSet, int i) throws SQLException {
    Company company = new Company();
    company.setId(resultSet.getInt("pk_company_id"));
    company.setTitle(resultSet.getString("title"));
    company.setTaxIdentificationNumber(resultSet.getString("tax_identification_number"));
    company.setAddress(resultSet.getString("address"));
    company.setPhoneNumber(resultSet.getString("phone_number"));
    return company;
  }
}
