package ru.aisa.springapp.mapper;

import com.vaadin.flow.data.binder.ValueContext;
import com.vaadin.flow.data.converter.LocalDateToDateConverter;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import ru.aisa.springapp.entity.Company;
import ru.aisa.springapp.entity.Employee;

public class EmployeeRowMapperWithCompany implements RowMapper<Employee> {

  @Override
  public Employee mapRow(ResultSet resultSet, int i) throws SQLException {
    Company company = new Company();
    company.setId(resultSet.getInt("pk_company_id"));
    company.setTitle(resultSet.getString("title"));
    company.setTaxIdentificationNumber(resultSet.getString("tax_identification_number"));
    company.setAddress(resultSet.getString("address"));
    company.setPhoneNumber(resultSet.getString("phone_number"));

    Employee employee = new Employee();
    employee.setId(resultSet.getInt("pk_employee_id"));
    employee.setFullName(resultSet.getString("full_name"));
    employee.setDateOfBirth(new LocalDateToDateConverter()
        .convertToPresentation(resultSet.getDate("date_of_birth"), new ValueContext()));
    employee.setEmail(resultSet.getString("email"));
    employee.setCompany(company);
    return employee;
  }
}
