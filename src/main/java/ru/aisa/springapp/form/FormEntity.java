package ru.aisa.springapp.form;

import ru.aisa.springapp.entity.AbstractEntity;

public interface FormEntity<E extends AbstractEntity> {

  void init();

  void validationFields();

  void save();

  void update();

  void edit(E e);
}
