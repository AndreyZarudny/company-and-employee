package ru.aisa.springapp.form;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment;
import com.vaadin.flow.component.orderedlayout.FlexComponent.JustifyContentMode;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import ru.aisa.springapp.component.ChangeHandler;
import ru.aisa.springapp.entity.Company;
import ru.aisa.springapp.repository.CompanyRepository;

@SpringComponent
@UIScope
public class CompanyForm extends Dialog implements FormEntity<Company> {

  CompanyRepository repository;
  Binder<Company> binder = new Binder<>(Company.class);

  Company company;

  TextField title = new TextField("Название");
  TextField taxIdentificationNumber = new TextField("ИНН");
  TextField address = new TextField("Адрес");
  TextField phoneNumber = new TextField("Телефон");

  Button save = new Button("Сохранить");
  Button close = new Button("Закрыть");

  VerticalLayout verticalLayout = new VerticalLayout(title, taxIdentificationNumber, address,
      phoneNumber);
  HorizontalLayout horizontalLayout = new HorizontalLayout(save, close);

  @Setter
  ChangeHandler changeHandler;

  @Autowired
  public CompanyForm(CompanyRepository repository) {
    this.repository = repository;
    init();
    validationFields();

    add(verticalLayout, horizontalLayout);
    binder.bindInstanceFields(this);

    close.addClickListener(e -> close());
    save.addClickListener(e -> {
      if (company.getTitle() != null && company.getTaxIdentificationNumber() != null &&
          company.getAddress() != null && company.getPhoneNumber() != null) {

        if (company.getId() == null) {
          save();
        } else {
          update();
        }
        close();

      } else {
        String message = "Одно или несколько полей заполнены неверно";
        new Notification(message, 10000, Position.TOP_CENTER).open();
      }
    });
  }

  @Override
  public void init() {
    setModal(true);
    setWidth("350px");
    setHeight("580px");

    verticalLayout.setAlignItems(Alignment.CENTER);
    verticalLayout.setJustifyContentMode(JustifyContentMode.CENTER);

    title.setWidth("280px");
    taxIdentificationNumber.setWidth("280px");
    address.setWidth("280px");
    phoneNumber.setWidth("280px");

    horizontalLayout.setAlignItems(Alignment.CENTER);
    horizontalLayout.setJustifyContentMode(JustifyContentMode.CENTER);
  }

  @Override
  public void validationFields() {
    binder.forField(title)
        .asRequired()
        .withValidator(text -> text.trim().length() >= 3,
            "Минимальная длина названия - 3 символа")
        .withValidator(text -> text.trim().length() <= 50,
            "Максимальная длина названия - 50 символов")
        .bind(Company::getTitle, Company::setTitle);

    binder.forField(taxIdentificationNumber)
        .asRequired()
        .withValidator(text -> isValid(text.trim()),
            "В строке ввода не должно быть посторонних символов")
        .withValidator(text -> text.length() == 10, "ИНН должен состоять из 10 цифр")
        .bind(Company::getTaxIdentificationNumber, Company::setTaxIdentificationNumber);

    binder.forField(address)
        .asRequired()
        .withValidator(text -> text.trim().length() >= 5,
            "Минимальная длина адреса - 5 символов")
        .withValidator(text -> text.trim().length() <= 100,
            "Максимальная длина адреса - 100 символов")
        .bind(Company::getAddress, Company::setAddress);

    binder.forField(phoneNumber)
        .asRequired()
        .withValidator(text -> isValid(text.trim()),
            "В строке ввода не должно быть посторонних символов")
        .withValidator(text -> text.trim().length() >= 7,
            "Минимальная длина номера телефона - 7 цифр")
        .withValidator(text -> text.trim().length() <= 15,
            "Максимальная длина номера телефона - 15 цифр")
        .bind(Company::getPhoneNumber, Company::setPhoneNumber);
  }

  @Override
  public void save() {
    repository.save(company);
    changeHandler.onChange();
  }

  @Override
  public void update() {
    repository.update(company);
    changeHandler.onChange();
  }

  @Override
  public void edit(Company entity) {
    if (entity.getId() != null) {
      company = repository.findById(entity.getId());
    } else {
      company = entity;
    }

    binder.setBean(company);
    open();
    title.focus();
  }

  public boolean isValid(String text) {
    try {
      Long.parseLong(text);
    } catch (Exception e) {
      return false;
    }
    return true;
  }
}
