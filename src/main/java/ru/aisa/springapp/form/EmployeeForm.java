package ru.aisa.springapp.form;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment;
import com.vaadin.flow.component.orderedlayout.FlexComponent.JustifyContentMode;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.validator.EmailValidator;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import java.time.LocalDate;
import java.util.Locale;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import ru.aisa.springapp.component.ChangeHandler;
import ru.aisa.springapp.entity.Company;
import ru.aisa.springapp.entity.Employee;
import ru.aisa.springapp.repository.CompanyRepository;
import ru.aisa.springapp.repository.EmployeeRepository;

@SpringComponent
@UIScope
public class EmployeeForm extends Dialog implements FormEntity<Employee> {

  EmployeeRepository repository;
  CompanyRepository companyRepository;
  Binder<Employee> binder = new Binder<>(Employee.class);

  Employee employee;

  TextField fullName = new TextField("ФИО");
  DatePicker dateOfBirth = new DatePicker("Дата рождения");
  EmailField email = new EmailField("Электронная почта");
  ComboBox<Company> company = new ComboBox<>("Company");

  Button save = new Button("Сохранить");
  Button close = new Button("Закрыть");

  VerticalLayout verticalLayout = new VerticalLayout(fullName, dateOfBirth, email, company);
  HorizontalLayout horizontalLayout = new HorizontalLayout(save, close);

  @Setter
  ChangeHandler changeHandler;

  @Autowired
  public EmployeeForm(EmployeeRepository repository, CompanyRepository companyRepository) {
    this.repository = repository;
    this.companyRepository = companyRepository;
    init();
    validationFields();

    add(verticalLayout, horizontalLayout);

    binder.bindInstanceFields(this);

    close.addClickListener(e -> close());
    save.addClickListener(e -> {
      if (employee.getFullName() != null && employee.getDateOfBirth() != null &&
          employee.getEmail() != null && employee.getCompany() != null) {

        if (employee.getId() == null) {
          save();
        } else {
          update();
        }
        close();

      } else {
        String message = "Одно или несколько полей заполнены неверно";
        new Notification(message, 10000, Position.TOP_CENTER).open();
      }
    });
  }

  @Override
  public void init() {
    setModal(true);
    setWidth("350px");
    setHeight("580px");

    dateOfBirth.setLocale(Locale.GERMAN);

    verticalLayout.setAlignItems(Alignment.CENTER);
    verticalLayout.setJustifyContentMode(JustifyContentMode.CENTER);

    fullName.setWidth("280px");
    dateOfBirth.setWidth("280px");
    email.setWidth("280px");
    company.setWidth("280px");

    company.setItems(companyRepository.findAll());
    company.setItemLabelGenerator(Company::getTitle);

    horizontalLayout.setAlignItems(Alignment.CENTER);
    horizontalLayout.setJustifyContentMode(JustifyContentMode.CENTER);
  }

  @Override
  public void validationFields() {
    binder.forField(fullName)
        .asRequired()
        .withValidator(text -> text.trim().length() >= 5,
            "Минимальная длина ФИО - 5 символов")
        .withValidator(text -> text.trim().length() <= 50,
            "Максимальная длина ФИО - 50 символов")
        .bind(Employee::getFullName, Employee::setFullName);

    binder.forField(dateOfBirth)
        .asRequired()
        .withValidator(date -> date.compareTo(LocalDate.now()) < 0,
            "Дата рождения не может быть позднее текущей")
        .bind(Employee::getDateOfBirth, Employee::setDateOfBirth);

    binder.forField(email)
        .asRequired()
        .withValidator(text -> text.trim().length() <= 50,
            "Максимальная длина электронной почты - 50 символов")
        .withValidator(new EmailValidator("Некорректный ввод"))
        .bind(Employee::getEmail, Employee::setEmail);

    binder.forField(company)
        .asRequired()
        .bind(Employee::getCompany, Employee::setCompany);
  }

  @Override
  public void save() {
    repository.save(employee);
    changeHandler.onChange();
  }

  @Override
  public void update() {
    repository.update(employee);
    changeHandler.onChange();
  }

  @Override
  public void edit(Employee entity) {
    if (entity.getId() != null) {
      employee = repository.findById(entity.getId());
    } else {
      employee = entity;
    }

    binder.setBean(employee);
    open();
    fullName.focus();
  }
}
