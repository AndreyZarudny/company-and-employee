package ru.aisa.springapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CompanyAndEmployeeApplication {

  public static void main(String[] args) {
    SpringApplication.run(CompanyAndEmployeeApplication.class, args);
  }

}
