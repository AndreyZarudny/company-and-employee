package ru.aisa.springapp.entity;

import lombok.Data;

@Data
public class Company extends AbstractEntity {

  private String title;
  private String taxIdentificationNumber;
  private String address;
  private String phoneNumber;
}
