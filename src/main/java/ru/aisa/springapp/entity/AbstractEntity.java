package ru.aisa.springapp.entity;

import lombok.Data;

@Data
public abstract class AbstractEntity {

  private Integer id;
}
