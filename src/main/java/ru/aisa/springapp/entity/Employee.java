package ru.aisa.springapp.entity;

import java.time.LocalDate;
import lombok.Data;

@Data
public class Employee extends AbstractEntity {

  private String fullName;
  private LocalDate dateOfBirth;
  private String email;

  private int companyId;
  private Company company;
  private String companyTitle;
  private String dateOfBirthString;
}
