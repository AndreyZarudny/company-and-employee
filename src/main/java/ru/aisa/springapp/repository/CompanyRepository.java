package ru.aisa.springapp.repository;

import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.aisa.springapp.dao.Dao;
import ru.aisa.springapp.entity.Company;
import ru.aisa.springapp.mapper.CompanyRowMapper;

@Slf4j
@Repository
public class CompanyRepository implements Dao<Company> {

  final String EDIT_COMPANY_MESSAGE = "Company with id: {} successfully edit";
  final String DELETE_COMPANY_MESSAGE = "Company with id: {} successfully deleted";
  final String ADD_COMPANY_MESSAGE = "Company with title: {} successfully added";

  @Autowired
  NamedParameterJdbcTemplate jdbcTemplate;

  @Override
  public void save(Company company) {
    final String sql =
        "INSERT INTO company (title, tax_identification_number, address, phone_number) "
            + "VALUES (:title, :taxIdentificationNumber, :address, :phoneNumber)";

    KeyHolder holder = new GeneratedKeyHolder();
    SqlParameterSource params = new MapSqlParameterSource()
//        .addValue("id", company.getId())
        .addValue("title", company.getTitle())
        .addValue("taxIdentificationNumber", company.getTaxIdentificationNumber())
        .addValue("address", company.getAddress())
        .addValue("phoneNumber", company.getPhoneNumber());

    try {
      int answer = jdbcTemplate.update(sql, params, holder);

      if (answer == 1) {
        log.info(ADD_COMPANY_MESSAGE, company.getTitle());

        String message = "Компания с названием " + company.getTitle() + " успешно добавлена";
        new Notification(message, 5000, Position.TOP_CENTER).open();
      }
    } catch (DataAccessException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void update(Company company) {
    final String sql = "UPDATE company SET "
        + "title = :title, tax_identification_number = :taxIdentificationNumber, "
        + "address = :address, phone_number = :phoneNumber "
        + "WHERE pk_company_id = :id";

    KeyHolder holder = new GeneratedKeyHolder();
    SqlParameterSource params = new MapSqlParameterSource()
        .addValue("id", company.getId())
        .addValue("title", company.getTitle())
        .addValue("taxIdentificationNumber", company.getTaxIdentificationNumber())
        .addValue("address", company.getAddress())
        .addValue("phoneNumber", company.getPhoneNumber());

    try {
      int answer = jdbcTemplate.update(sql, params, holder);

      if (answer == 1) {
        log.info(EDIT_COMPANY_MESSAGE, company.getId());

        String message = "Компания с идентификатором " + company.getId() + " успешно изменена";
        new Notification(message, 5000, Position.TOP_CENTER).open();
      }
    } catch (DataAccessException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void delete(Company company) {
    try {
      final String sql = "DELETE FROM company WHERE pk_company_id = :id";

      SqlParameterSource params = new MapSqlParameterSource()
          .addValue("id", company.getId());

      int answer = jdbcTemplate.update(sql, params);

      if (answer == 1) {
        log.info(DELETE_COMPANY_MESSAGE, company.getId());

        String message = "Компания с идентификатором " + company.getId() + " успешно удалена";
        new Notification(message, 5000, Position.TOP_CENTER).open();
      }
    } catch (DataAccessException e) {
      e.printStackTrace();
    }
  }

  @Override
  public Company findById(int id) {
    final String sql = "SELECT * FROM company WHERE pk_company_id = :id";

    SqlParameterSource params = new MapSqlParameterSource()
        .addValue("id", id);

    Company company = null;

    try {
      company = jdbcTemplate.queryForObject(sql, params, new CompanyRowMapper());
    } catch (DataAccessException e) {
      e.printStackTrace();
    }

    return company;
  }

  @Override
  public List<Company> findAll() {
    final String sql = "SELECT * FROM company ORDER BY pk_company_id ASC";

    List<Company> companyList = null;

    try {
      companyList = jdbcTemplate.query(sql, new CompanyRowMapper());
    } catch (DataAccessException e) {
      e.printStackTrace();
    }

    return companyList;
  }

  @Override
  public List<Company> findByData(String title) {
    String sql = "SELECT * FROM company WHERE title LIKE CONCAT('%', :title, '%')";

    SqlParameterSource params = new MapSqlParameterSource()
        .addValue("title", title);

    List<Company> companyList = null;

    try {
      companyList = jdbcTemplate.query(sql, params, new CompanyRowMapper());
    } catch (DataAccessException e) {
      e.printStackTrace();
    }

    return companyList;
  }
}
