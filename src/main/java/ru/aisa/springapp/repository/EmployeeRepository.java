package ru.aisa.springapp.repository;

import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.aisa.springapp.dao.Dao;
import ru.aisa.springapp.entity.Employee;
import ru.aisa.springapp.mapper.EmployeeRowMapper;
import ru.aisa.springapp.mapper.EmployeeRowMapperWithCompany;

@Slf4j
@Repository
public class EmployeeRepository implements Dao<Employee> {

  final String EDIT_EMPLOYEE_MESSAGE = "Employee with id: {} successfully edit";
  final String DELETE_EMPLOYEE_MESSAGE = "Employee with id: {} successfully deleted";
  final String ADD_EMPLOYEE_MESSAGE = "Employee with name: {} successfully added";

  @Autowired
  NamedParameterJdbcTemplate jdbcTemplate;

  @Override
  public void save(Employee employee) {
    final String sql =
        "INSERT INTO employee (full_name, date_of_birth, email, fk_company_id) "
            + "VALUES (:fullName, :dateOfBirth, :email, :companyId)";

    KeyHolder holder = new GeneratedKeyHolder();
    SqlParameterSource params = new MapSqlParameterSource()
        .addValue("fullName", employee.getFullName())
        .addValue("dateOfBirth", employee.getDateOfBirth())
        .addValue("email", employee.getEmail())
        .addValue("companyId", employee.getCompany().getId());

    try {
      int answer = jdbcTemplate.update(sql, params, holder);

      if (answer == 1) {
        log.info(ADD_EMPLOYEE_MESSAGE, employee.getFullName());

        String message = "Сотрудник с именем " + employee.getFullName() + " успешно добавлен";
        new Notification(message, 5000, Position.TOP_CENTER).open();
      }
    } catch (DataAccessException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void update(Employee employee) {
    final String sql = "UPDATE employee SET full_name = :fullName, date_of_birth = :dateOfBirth, "
        + "email = :email, fk_company_id = :companyId "
        + "WHERE pk_employee_id = :id";

    KeyHolder holder = new GeneratedKeyHolder();
    SqlParameterSource params = new MapSqlParameterSource()
        .addValue("id", employee.getId())
        .addValue("fullName", employee.getFullName())
        .addValue("dateOfBirth", employee.getDateOfBirth())
        .addValue("email", employee.getEmail())
        .addValue("companyId", employee.getCompany().getId());

    try {
      int answer = jdbcTemplate.update(sql, params, holder);

      if (answer == 1) {
        log.info(EDIT_EMPLOYEE_MESSAGE, employee.getId());

        String message = "Сотрудник с идентификатором " + employee.getId() + " успешно изменён";
        new Notification(message, 5000, Position.TOP_CENTER).open();
      }
    } catch (DataAccessException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void delete(Employee employee) {
    try {
      final String sql = "DELETE FROM employee WHERE pk_employee_id = :id";

      SqlParameterSource params = new MapSqlParameterSource()
          .addValue("id", employee.getId());

      int answer = jdbcTemplate.update(sql, params);

      if (answer == 1) {
        log.info(DELETE_EMPLOYEE_MESSAGE, employee.getId());

        String message = "Сотрудник с идентификатором " + employee.getId() + " успешно удалён";
        new Notification(message, 5000, Position.TOP_CENTER).open();
      }
    } catch (DataAccessException e) {
      e.printStackTrace();
    }
  }

  @Override
  public Employee findById(int id) {
    final String sql = "SELECT e.pk_employee_id, e.full_name, e.date_of_birth, e.email, "
        + "c.pk_company_id, c.title, c.tax_identification_number, c.address, c.phone_number "
        + "FROM employee AS e join company AS c on e.fk_company_id = c.pk_company_id "
        + "WHERE e.pk_employee_id = :id";

    SqlParameterSource params = new MapSqlParameterSource()
        .addValue("id", id);

    Employee employee = null;

    try {
      employee = jdbcTemplate.queryForObject(sql, params, new EmployeeRowMapperWithCompany());
    } catch (DataAccessException e) {
      e.printStackTrace();
    }

    return employee;
  }

  @Override
  public List<Employee> findAll() {
    final String sql = "SELECT e.pk_employee_id, e.full_name, e.date_of_birth, e.email, c.title "
        + "FROM employee AS e JOIN company AS c ON e.fk_company_id = c.pk_company_id "
        + "ORDER BY e.pk_employee_id ASC";

    List<Employee> employeeList = null;

    try {
      employeeList = jdbcTemplate.query(sql, new EmployeeRowMapper());
    } catch (DataAccessException e) {
      e.printStackTrace();
    }

    return employeeList;
  }

  @Override
  public List<Employee> findByData(String name) {
    final String sql = "SELECT e.pk_employee_id, e.full_name, e.date_of_birth, e.email, c.title "
        + "FROM employee AS e JOIN company AS c ON e.fk_company_id = c.pk_company_id "
        + "WHERE full_name LIKE CONCAT('%', :name, '%')";

    SqlParameterSource params = new MapSqlParameterSource()
        .addValue("name", name);

    List<Employee> employeeList = null;

    try {
      employeeList = jdbcTemplate.query(sql, params, new EmployeeRowMapper());
    } catch (DataAccessException e) {
      e.printStackTrace();
    }

    return employeeList;
  }

  public List<Employee> findByCompany(String title) {
    final String sql = "SELECT e.pk_employee_id, e.full_name, e.date_of_birth, e.email, c.title "
        + "FROM employee AS e JOIN company AS c ON e.fk_company_id = c.pk_company_id "
        + "WHERE title LIKE (:title)";

    SqlParameterSource params = new MapSqlParameterSource()
        .addValue("title", title);

    List<Employee> employeeList = null;

    try {
      employeeList = jdbcTemplate.query(sql, params, new EmployeeRowMapper());
    } catch (DataAccessException e) {
      e.printStackTrace();
    }

    return employeeList;
  }
}
