package ru.aisa.springapp.component;

public interface ChangeHandler {

  void onChange();
}
